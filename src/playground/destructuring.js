

// Object destructuring 

// const person = {
//     name: 'Alex',
//     age: 35,
//     location: {
//         city: 'Kansas City',
//         temp: 55
//     }
// }
// const { name = 'none', age } = person;
// console.log(`${name} is ${age}`);

// const { city, temp: temperature } = person.location;

// if (city && temperature) {
//     console.log(`It's ${temperature} in ${city}`)
// }

// const book = {
//     title: 'Ego is the Enemy',
//     author: 'Ryan Holiday',
//     publisher: {
//         name: 'Penguin'
//     }
// }

// const { name: publisherName = 'none' } = book.publisher

// console.log(publisherName);


// Array destructuring 

const address = ['1299 S Good Street', 'KC', 'MO', '65721'];

const [, city, state,] = address;

console.log(`You are in ${city} ${state}.`)

const item = ['Coffee (iced)', '2.00', '3.50', '2.75'];

const [type, , medium,] = item;

console.log(` A medium ${type} costs ${medium}`)