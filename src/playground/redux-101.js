import { createStore } from 'redux';

// Action generators - functions that return action objects


const incrementCount = ({ incrementBy = 1 } = {}) => ({
    type: 'INCREMENT',
    incrementBy
});

const decrementCount = ({ decrementBy = 1 } = {}) => ({
    type: 'DECREMENT',
    decrementBy
});

const setCount = ({ setBy = 69 } = {}) => ({
    type: 'SET',
    setBy
});

const resetCount = ({ resetTo = 0 } = {}) => ({
    type: 'RESET',
    resetTo
});

/*Reducers
 1. Reducers are pure function, which are function
 that have out that is only determined by the input

 2. Never change state or action. Do not mutate the state directly, rather just
 return an updated object.
*/
const countReducer = (state = { count: 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            };
        case 'DECREMENT':
            return {
                count: state.count - action.decrementBy
            }
        case 'SET':
            return {
                count: action.setBy
            }
        case 'RESET':
            return {
                count: action.resetTo
            }
        default:
            return state;
    }
}

const store = createStore(countReducer);

const unsubscribe = store.subscribe(() => {
    console.log(store.getState());
})

/* Actions -> 
    An Action is an object that gets sent to the store
    that describes the type of action that we want to take.
    *Increment, decrement, reset
*/



// I would like to increment the count
// store.dispatch(
//     {
//         type: 'INCREMENT',
//         incrementBy: 5
//     }

// )

store.dispatch(incrementCount({ incrementBy: 5 }))

// I would like to decrement the count
// store.dispatch(
//     {
//         type: 'DECREMENT'
//     }
// )

store.dispatch(decrementCount({ decrementBy: 3 }))

// store.dispatch(
//     {
//         type: 'SET',
//         count: 101
//     }
// )

store.dispatch(setCount({ setBy: 420 }));

store.dispatch(resetCount({ resetTo: 0 }));