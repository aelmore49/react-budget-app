
//Update Filter Text Generator
export const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text
});

// Update Filter Sort by Amount Generator
export const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
});

// Update Filter Sort by Date Generator
export const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});

// Update Filter Set Start Date Generator
export const setStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
});

// Update Filter Set End Date Generator
export const setEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
})